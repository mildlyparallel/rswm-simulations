#include <cassert>

#include "Runner.hh"

#include "Strategy.hh"
#include "SystemStationary.hh"

Runner::Runner()
{  }

Runner::~Runner()
{  }

void Runner::add(Strategy *strategy)
{
	m_strategies.push_back(strategy);
}

void Runner::open(const std::string &filepath)
{
	assert(!m_out.is_open());

	m_out = std::ofstream(filepath);
}

void Runner::print_header()
{
	m_out << "id,size";

	for (auto &s : m_strategies)
		m_out << "," << s->name();

	m_out << std::endl;
}

void Runner::run(SystemStationary &system)
{
	m_out << system.get_id();
	m_out << "," << system.size();

	for (auto &s : m_strategies) {
		s->reset();

		s->add(system);

		s->start();

		m_out << "," << s->get_makespan();
	}

	m_out << std::endl;
}
