#include <cmath>

#include "SystemDynamic.hh"

SystemDynamic::SystemDynamic(size_t n)
: System(n)
{  }

SystemDynamic::~SystemDynamic()
{  }

void SystemDynamic::init_base_rate(Task::rate_fn rate)
{
	for (auto &t : m_tasks)
		t.set_base_rate_fn(rate);
}

void SystemDynamic::init_rate_scale(std::function<Task::rate_fn (taskset)> scale_gen_fn)
{
	size_t n = 1 << m_tasks.size();

	for (size_t j = 1; j < n; ++j) {
		taskset ts(j);

		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;

			auto scale_fn = scale_gen_fn(ts);

			m_tasks[i].set_rate_scale_fn(ts, scale_fn);
		}
	}
}

void SystemDynamic::init_rate_scale(std::function<Task::rate_fn (taskset, const Task &)> scale_gen_fn)
{
	size_t n = 1 << m_tasks.size();

	for (size_t j = 1; j < n; ++j) {
		taskset ts(j);

		for (size_t i = 0; i < m_tasks.size(); ++i) {
			if (!ts[i])
				continue;

			auto scale_fn = scale_gen_fn(ts, m_tasks[j]);

			m_tasks[i].set_rate_scale_fn(ts, scale_fn);
		}
	}

}
