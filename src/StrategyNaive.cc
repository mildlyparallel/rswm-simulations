#include "StrategyNaive.hh"

StrategyNaive::StrategyNaive()
{  }

StrategyNaive::~StrategyNaive()
{  }

void StrategyNaive::start()
{
	while (!all_complited()) {
		taskset ts = get_schedulable();
		run(ts);
	}
}

const char *StrategyNaive::name() const
{
	return "naive";
}
