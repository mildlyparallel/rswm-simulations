#include <iostream>
#include <random>
#include <thread>
#include <queue>
#include <mutex>
#include <sstream>

#include "SystemStationary.hh"
#include "Runner.hh"
#include "Config.hh"

#include "StrategySequential.hh"
#include "StrategyNaive.hh"
#include "StrategyOPT.hh"
#include "StrategyFCS.hh"
#include "StrategySearch.hh"

void fill_system_rnd(
	const Config &config,
	SystemStationary &sys,
	std::default_random_engine &gen
) {
	std::uniform_int_distribution<int> work_distr(config.min_work, config.max_work);

	std::uniform_real_distribution<double> slope_coeff(
		config.min_slope_scale,
		config.max_slope_scale
	);

	double slope_distr_min[Task::MAX_TASKS];

	for (size_t i = 0; i < sys.size(); ++i)
		slope_distr_min[i] = slope_coeff(gen);

	sys.init_base_rate(1);

	sys.init_scale_rnd([&] (size_t task_id) -> double {
		std::uniform_real_distribution<double> slope_coeff(slope_distr_min[task_id], 1);
		return slope_coeff(gen);
	});

	sys.init_total_work([&] (const Task &) {
		return work_distr(gen);
	});
}

void worker_fn(
	size_t wid,
	const Config &config,
	std::queue<SystemStationary> &systems,
	std::mutex &syslock
)
{
	Runner runner;

	StrategyOPT stg_offline_cmax_opt;
	runner.add(&stg_offline_cmax_opt);

	StrategyFCS stg_offline_tp_opt;
	runner.add(&stg_offline_tp_opt);

	StrategySequential stg_seq;
	runner.add(&stg_seq);

	static const size_t nr_q = 6;

	static const char *suff_q[nr_q] = {
		"0.55",
		"0.65",
		"0.75",
		"0.85",
		"0.95",
		"0.99"
	};

	static const double quartile[nr_q] = {
		0.1256613,
		0.3853205,
		0.6744898,
		1.0364334,
		1.6448536,
		2.3263479
	};

	StrategySearch stg_tp_ucb[nr_q];
	StrategySearch stg_tp_ei[nr_q];
	StrategySearch stg_tp_pi[nr_q];

	for (size_t i = 0; i < nr_q; ++i) {
		stg_tp_ucb[i].set_acc_fun(StrategySearch::UCB);
		stg_tp_ucb[i].set_quartile(quartile[i]);
		stg_tp_ucb[i].add_name_suffix(suff_q[i]);
		runner.add(&stg_tp_ucb[i]);
	}

	for (size_t i = 0; i < nr_q; ++i) {
		stg_tp_ei[i].set_acc_fun(StrategySearch::EI);
		stg_tp_ei[i].set_quartile(quartile[i]);
		stg_tp_ei[i].add_name_suffix(suff_q[i]);
		runner.add(&stg_tp_ei[i]);
	}

	for (size_t i = 0; i < nr_q; ++i) {
		stg_tp_pi[i].set_acc_fun(StrategySearch::PI);
		stg_tp_pi[i].set_quartile(quartile[i]);
		stg_tp_pi[i].add_name_suffix(suff_q[i]);
		runner.add(&stg_tp_pi[i]);
	}

	std::stringstream out;
	out << config.output << "/results-" << wid << ".csv";

	runner.open(out.str());

	if (wid == 0)
		runner.print_header();

	while (true) {
		SystemStationary sys;

		{
			std::lock_guard lk(syslock);
			if (systems.empty()) {
				std::cout << "Worker " << wid << " exit" << std::endl;
				break;
			}

			sys = systems.front();
			systems.pop();

			std::cout << "Worker " << wid << " took #" << sys.get_id() << " left " <<  systems.size() << std::endl;
		}

		runner.run(sys);
	}

}

int main(int argc, const char **argv)
{
	Config config;
	config.configure(argc, argv);

	std::default_random_engine gen;
	gen.seed(config.seed);

	size_t sys_id = 1;
	std::queue<SystemStationary> systems;
	for (size_t nr_tasks = config.min_tasks; nr_tasks < config.max_tasks; ++nr_tasks) {
		for (size_t i = 0; i < config.nr_runs; ++i) {
			systems.emplace(nr_tasks);
			systems.back().set_id(sys_id++);
			fill_system_rnd(config, systems.back(), gen);
			systems.back().write(config.output);
			systems.back().write_by_task(config.output);
			systems.back().write_by_comb(config.output);
		}
	}

	std::cout << "Total simulations:" << systems.size() << std::endl;

	std::mutex systems_lock;

	std::vector<std::thread> threads;

	for (size_t i = 0; i < std::thread::hardware_concurrency(); ++i) {
		threads.emplace_back(
			worker_fn,
			i,
			std::ref(config),
			std::ref(systems),
			std::ref(systems_lock)
		);
	}

	for (auto &th : threads) {
		th.join();
	}

	std::cerr << "\nDone.\n";

	return 0;
}
