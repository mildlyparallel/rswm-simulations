#include <iostream>
#include <iomanip>
#include <fstream>
#include <filesystem>

#include <Dashh.hh>
#include <Utils.hh>

#include "Config.hh"
#include "Task.hh"

void Config::configure(int argc, const char **argv)
{
	using namespace dashh;

	Command cmd;
	cmd << Option(nr_runs, "-r, --runs", "N", "Number of input systems generated for each paramter values set");
	cmd << Option(output, "-o, --output", "DIR", "Output directory for input systems and simul results");
	cmd << Option(seed, "--seed", "N", "Random seed value");
	cmd << Option(min_tasks, "--tasks-min", "tasks", "Min value for the range of the number of task");
	cmd << Option(max_tasks, "--tasks-max", "tasks", "Max value for the range of the number of task");
	cmd << Option(min_work, "--work-min", "W", "Min value for work units distribution");
	cmd << Option(max_work, "--work-max", "W", "Max value for work units distribution");
	cmd << Option(min_slope_scale, "--slope-scale-min", "K", "Min value for speed distribution");
	cmd << Option(max_slope_scale, "--slope-scale-max", "K", "Max value for speed distribution");
	cmd << Flag(PageHelp(), "-h");
	Dashh t(&cmd);
	t.parse(argc, argv);

	if (max_tasks > Task::MAX_TASKS) {
		std::cerr << max_tasks << " tasks is too much" << std::endl;
		std::exit(EXIT_FAILURE);
	}

	if (max_tasks == min_tasks)
		max_tasks = min_tasks + 1;

	std::filesystem::create_directory(output);

	std::cout << "slope_noise      = " << "Norm(" << slope_noise_mean << ", " << slope_noise_dev << ")" << std::endl;
	std::cout << "slope_scale      = " << "Unif(" << min_slope_scale << ", " << max_slope_scale << ")" << std::endl;
	std::cout << "nr_runs          = " << nr_runs << std::endl;
	std::cout << "tasks            = " << min_tasks << ":" << max_tasks << std::endl;
	std::cout << "work             = " << "Unif(" << min_work << ", " << max_work << ")" << std::endl;
	std::cout << "output           = " << output << std::endl;

	write();
}

void Config::write()
{
	std::ofstream out(output + "/input.dat");

	out << "max_tasks " << max_tasks << "\n";
	out << "min_tasks " << min_tasks << "\n";
	out << "nr_runs " << nr_runs << "\n";
	out << "seed " << seed << "\n";
	out << "max_slope_scale " << max_slope_scale << "\n";
	out << "min_slope_scale " << min_slope_scale << "\n";
	out << "slope_noise_mean " << slope_noise_mean << "\n";
	out << "slope_noise_dev " << slope_noise_dev << "\n";
	out << "min_work " << min_work << "\n";
	out << "max_work " << max_work << "\n";
}

