#include <cassert>
#include <iostream>

#include "Strategy.hh"
#include "System.hh"

Strategy::Strategy()
{  }

Strategy::~Strategy()
{  }

void Strategy::add(const System &sys)
{
	reset();

	for (auto &t : sys.tasks()) {
		m_tasks.push_back(t);
	}
}

double Strategy::now()
{
	return m_now;
}

size_t Strategy::size() const
{
	return m_tasks.size();
}

bool Strategy::any_complited(taskset ts) const
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		if (m_tasks[i].complited())
			return true;
	}

	return false;
}

bool Strategy::all_complited(taskset ts) const
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;

		if (!m_tasks[i].complited())
			return false;
	}

	return true;
}

bool Strategy::all_complited() const
{
	for (auto &task : m_tasks) {
		if (!task.complited())
			return false;
	}

	return true;
}

bool Strategy::all_released(taskset ts) const
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (!ts[i])
			continue;
		
		if (!m_tasks[i].released())
			return false;
	}

	return true;
}

Strategy::taskset Strategy::get_schedulable() const
{
	taskset ret;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].complited())
			continue;
		if (!m_tasks[i].released())
			continue;
		ret[i] = 1;
	}

	return ret;
}

size_t Strategy::get_next_schedulable() const
{
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].complited())
			continue;
		if (!m_tasks[i].released())
			continue;
		return i;
	}

	return m_tasks.size() + 1;
}

Strategy::taskset Strategy::get_complited() const
{
	taskset ret;

	for (size_t i = 0; i < m_tasks.size(); ++i) {
		if (m_tasks[i].complited())
			ret[i] = 1;
	}

	return ret;
}

double Strategy::get_makespan() const
{
	double makespan = 0;

	for (auto &t : m_tasks) {
		if (t.get_complition_time() > makespan)
			makespan = t.get_complition_time();
	}

	return makespan;
}

double Strategy::get_flowtime() const
{
	double flowtime = 0;

	for (auto &t : m_tasks) {
		if (t.get_complition_time() > flowtime)
			flowtime += t.get_complition_time();
	}

	return flowtime;
}

void Strategy::open_log(const std::string &file)
{
	assert(!m_log.is_open());

	m_log.open(file);

	m_log << "time";
	for (size_t i = 0; i < m_tasks.size(); ++i)
		m_log << ",task" << (i+1);

	m_log << std::endl;
}

void Strategy::log(taskset ts)
{
	if (!m_log.good())
		return;

	m_log << now();
	
	for (size_t i = 0; i < m_tasks.size(); ++i) {
		m_log << ",";
		if (ts[i])
			m_log << m_tasks[i].get_rate_last();
		else
			m_log << 0;
	}

	m_log << std::endl;
}

void Strategy::reset()
{
	m_tasks.clear();
	m_now = 0;
}

