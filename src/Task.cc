#include <cassert>
#include <iostream>
#include <cmath>

#include "Task.hh"

Task::Task()
{  }

Task::~Task()
{  }

void Task::set_id(size_t i)
{
	m_id = i;
}

size_t Task::get_id() const
{
	return m_id;
}

void Task::set_work_total(double w)
{
	m_work_total = w;
}

double Task::get_work_total() const
{
	return m_work_total;
}

void Task::set_base_rate_fn(rate_fn fn)
{
	m_base_rate_fn = fn;
}

double Task::get_base_rate(double w) const
{
	assert(m_base_rate_fn);
	return m_base_rate_fn(w);
}

void Task::set_rate_scale_fn(taskset tasks, rate_fn fn)
{
	m_rate_scale[tasks.to_ullong()] = fn;
}

double Task::get_rate_scale(taskset tasks, double w) const
{
	auto it = m_rate_scale.find(tasks.to_ullong());
	if (it == m_rate_scale.end())
		return 0;

	return it->second(w);
}

double Task::get_rate(taskset tasks, double w) const
{
	double rate = get_rate_scale(tasks, w) * get_base_rate(w);
	return rate;
}

void Task::set_release_time(double r)
{
	m_release_time = r;
}

double Task::get_release_time() const
{
	return m_release_time;
}
