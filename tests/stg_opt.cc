#include <iomanip>
#include <cmath>
#include "gtest/gtest.h"

#include "StrategyOPT.hh"
#include "StrategyFCS.hh"

#include "common.hh"

using namespace ::testing;

TEST(StrategyCmaxOptimal, solve_1) {
	SystemStationary tasks = get_stationary_1();

	StrategyOPT stg;
	stg.add(tasks);
	stg.start();

	EXPECT_NEAR(stg.get_makespan(), 626.25, 1e-6);
}

