#include "gtest/gtest.h"

#include "StrategyFCS.hh"

#include "common.hh"

using namespace ::testing;

TEST(StrategyFCSTest, solve_1)
{
	SystemStationary tasks = get_stationary_1();

	StrategyFCS stg;
	stg.add(tasks);
	stg.open_log("tp-fcs-log.csv");
	stg.start();

	std::cerr << "stg = " << stg.get_makespan() << std::endl;
}

