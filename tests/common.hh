#pragma once

#include "SystemStationary.hh"

SystemStationary get_stationary_1()
{
	static const size_t nr_tasks = 4;
	SystemStationary tasks(nr_tasks);
	tasks.init_base_rate(1);

	auto rate_fn = [&] (Task::taskset ts) {
		return 1. - 0.2 * (ts.count() - 1);
	};

	tasks.init_rate_scale(rate_fn);

	double work_total[nr_tasks] = {
		90, 226, 337, 425
	};

	for (size_t i = 0; i < nr_tasks; ++i)
		tasks.init_total_work(i, work_total[i]);

	return tasks;
}
