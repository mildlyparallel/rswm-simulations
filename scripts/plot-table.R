#!/usr/bin/env Rscript

library("optparse")

option_list = list(
	make_option(
		c("-i", "--input"),
		type="character",
		default='table.csv',
		help="Table data",
		metavar="character"
	),

	make_option(
		c("-o", "--output"),
		type="character",
		default="table.png",
		help="Output image [default= %default]",
		metavar="character"
	),

	make_option(
		c("-t", "--title"),
		type="character",
		default="",
		help="Sintitle [default= %default]",
		metavar="character"
	)
);

opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

cat('Reading', opt$input, '\n')

data <- read.csv(opt$input, check.names = F)

nr_rows <- dim(data)[1]
nr_cols <- dim(data)[2]
nr_tasks <- nr_rows / 4
nr_comb <- nr_cols - 1 

plot_table <- function(taskdata, main) {
	max_y <- max(taskdata, na.rm=T)
	plot(
		taskdata$real,
		col  = 2,
		pch  = 16,
		cex  = 1.2,
		xaxt = "n",
		xlim = c(1, nr_cols),
		ylim = c(0, max_y),
		xlab = 'Combination',
		ylab = 'Slowdown',
		main = main
	)

	xtick <- 1:nr_comb

	axis(side = 1, labels = FALSE, at=xtick)

	text(
		x = xtick,
		y = par("usr")[3] - 0.05 * max_y,
		cex = 0.8,
		srt = 90,
		pos = 1,
		labels = row.names(taskdata),
		xpd = NA
	)

	fillcol <- rgb(0, 0, 0, 0.1)

	for (i in 1:nr_comb) {
		dx <- 0.2
		y <- c(taskdata$min[i], taskdata$max[i], taskdata$max[i], taskdata$min[i])
		x <- i + c(-dx, -dx, dx, dx)
		lines(
			c(i-dx, i+dx),
			c(taskdata$mean[i], taskdata$mean[i]),
			col = rgb(0, 0, 0, 0.8)
		)
		polygon(x, y, col = fillcol)
	}

	legend(
		'bottomleft',
		legend=c(
			'Real data',
			'Mean estimate'
		),
		pch = c(16, NA),
		# cex = c(1.2, NA, NA),
		lwd = c(NA, 1),
		col = c(2, 1),
	)
}

sumdata <- data.frame(
	real = as.numeric(colSums(data[seq(4, nr_rows, 4), -1])),
	min  = as.numeric(colSums(data[seq(3, nr_rows, 4), -1])),
	mean = as.numeric(colSums(data[seq(2, nr_rows, 4), -1])),
	max  = as.numeric(colSums(data[seq(1, nr_rows, 4), -1]))
)

row.names(sumdata) <- colnames(data)[-1]

cat('Writing', opt$output, '\n')

png(opt$output, width=nr_tasks * 380, height=900, pointsize=18)

layout(matrix(
	c(
		rep(1, nr_tasks),
		1 + (1:nr_tasks)
	),
	nrow  = 2,
	ncol  = nr_tasks,
	byrow = TRUE
))

if (opt$title == "") {
	main <- 'Combination speed'
} else {
	main <- opt$title
}

plot_table(sumdata, main)

for (i in 1:nr_tasks) {
	row_id <- 4 * (i - 1) + 1
	scale_min <- as.numeric(data[row_id, -1])
	scale_min[scale_min == 0] <- NA
	scale_mean <- as.numeric(data[row_id + 1, -1])
	scale_mean[scale_mean == 0] <- NA
	scale_max <- as.numeric(data[row_id + 2, -1])
	scale_max[scale_max == 0] <- NA
	scale_real <- as.numeric(data[row_id + 3, -1])
	scale_real[scale_real == 0] <- NA

	taskdata <- data.frame(
		real = scale_real,
		min  = scale_min,
		mean = scale_mean,
		max  = scale_max
	)

	row.names(taskdata) <- colnames(data)[-1]

	plot_table(taskdata, paste('Task', i, 'speed'))
}


dev.off()
