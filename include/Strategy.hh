#pragma once

#include <vector>
#include <fstream>

#include "ScheduledTask.hh"

class System;

class Strategy
{
public:
	using taskset = Task::taskset;

	Strategy();

	virtual ~Strategy();

	virtual void add(const System &sys);

	virtual void start() = 0;

	double get_makespan() const;

	double get_flowtime() const;

	void open_log(const std::string &file);

	virtual const char *name() const = 0;

	virtual void reset();

protected:
	bool all_released(taskset ts) const;

	double now();

	size_t size() const;

	bool any_complited(taskset ts) const;

	bool all_complited(taskset ts) const;

	bool all_complited() const;

	size_t get_next_schedulable() const;

	taskset get_schedulable() const;

	taskset get_complited() const;

	void log(taskset ts);

	double m_now = 0;

	std::vector<ScheduledTask> m_tasks;

private:

	std::ofstream m_log;
};

