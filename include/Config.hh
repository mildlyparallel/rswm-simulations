#pragma once

class Config
{
public:
	size_t max_tasks = 5;
	size_t min_tasks = 3;
	size_t nr_runs = 2;
	int seed = 123;
	std::string output = "results";

	double max_slope_scale = 1;
	double min_slope_scale = 0;
	double slope_noise_mean = 0;
	double slope_noise_dev = 0.01;
	double min_work = 100;
	double max_work = 200;
	double confidence_scale = 0.2;

	void configure(int argc, const char **argv);

	void write();

protected:

private:

};
