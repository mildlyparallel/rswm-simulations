#pragma once

#include <vector>
#include <array>
#include <set>

#include "StrategyOffline.hh"
#include "SystemStationary.hh"

class StrategySearch : public StrategyOffline
{
public:
	StrategySearch();

	virtual ~StrategySearch();

	virtual void start();

	virtual const char *name() const;

	void solve(const SystemStationary &tasks);

	enum AccFun {
		UCB = 0,
		PI,
		EI,
	};

	void set_acc_fun(AccFun f);

	void set_quartile(double q);

	void add_name_suffix(const std::string &s);

	void set_explore_coeff(double e);

protected:

private:
	inline static const constexpr double time_unit = 10;

	struct TaskEstimate {
		double sigma = 0;
		double scale_min = 0;
		double scale_max = 0;
		double scale = 0;
		bool exact = false;

		void set(double r) {
			sigma = 0;
			scale_min = r;
			scale_max = r;
			scale = r;
			exact = true;
		}
	};

	void init();

	void write_estimate(const std::string &fileout = "tp-sampling.csv") const;

	void collect_all_ideal();

	void run_and_measure(taskset ts);

	void interpolate_missing();

	void interpolate_combination(size_t task_id, taskset ts);

	void interpolate_between(
		size_t task_id,
		taskset ts,
		taskset ts_min,
		taskset ts_max
	);

	double find_max_speed() const;

	double upper_confidence_bound(taskset ts) const;

	double prob_of_improvement(taskset ts) const;

	double expected_improvement(taskset ts) const;

	taskset find_next() const;

	static double interploate_liniear(
		double x,
		double x_l,
		double x_r,
		double y_l,
		double y_r
	);

	static bool is_subset(taskset ts_min, taskset ts_max);

	static double dnorm(double x, double a, double v);

	static double pnorm(double x);

	using Combinations = std::vector<TaskEstimate>;

	std::array<Combinations, Task::MAX_TASKS> m_estimate;

	std::array<double, Task::MAX_TASKS> m_ideal_rate;

	std::vector<taskset> m_sampled_combinations;

	double m_last_scale = 0;
	taskset m_last_comb = 0;

	AccFun m_acc_fun = UCB;

	double m_quratile = 0.1;

	double m_explore = 0;

	std::string m_name = "unknown";
};

