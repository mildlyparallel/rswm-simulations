#pragma once

#include "StrategyOffline.hh"

class StrategyRoundRobin : public StrategyOffline
{
public:
	StrategyRoundRobin();

	virtual ~StrategyRoundRobin();

	virtual void start();

	virtual const char *name() const;

	void set_time_unit(double time_unit);

	double get_estimate() const;

protected:

private:
	double m_time_unit;

};
