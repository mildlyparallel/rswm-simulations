#pragma once

#include <functional>
#include <vector>

#include "System.hh"

class SystemStationary : public System
{
public:
	SystemStationary();

	SystemStationary(size_t n);

	virtual ~SystemStationary();

	void init_base_rate(double r, double dr = 0);

	void init_base_rate(std::function<double (const Task &)> rate_gen_fn);

	void init_rate_scale(std::function<double (taskset)> scale_gen_fn);

	void init_rate_scale(std::function<double (taskset, const Task &)> scale_gen_fn);

	void set_rate_scale(size_t task_id, taskset ts, double scale);

	void init_scale_rnd(std::function<double (size_t task_id)> scale_fn);

	void print() const;

	std::vector<std::pair<taskset, double>> solve() const;

	void write(const std::string &outdir) const;

	void write_by_task(const std::string &outdir) const;

	void write_by_comb(const std::string &outdir) const;

protected:

private:
	void print_val(const std::string &label, std::function<double ()> fn) const;

	void print_row(const std::string &label, std::function<double (const Task &task)> fn) const;

	void print_comb(const std::string &label, std::function<double (const Task &task, taskset)> fn) const;

	void init_scale_rnd(
		size_t task_id,
		std::function<double(size_t)> scale_fn
	);

	double find_min_scale(size_t task_id, taskset ts) const;

};

