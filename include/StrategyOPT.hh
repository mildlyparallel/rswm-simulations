#pragma once

#include <vector>

#include "StrategyOffline.hh"
#include "SystemStationary.hh"

class StrategyOPT : public StrategyOffline
{
public:
	StrategyOPT();

	virtual ~StrategyOPT();

	virtual const char *name() const;

	virtual void start();

protected:

private:
	std::vector<std::pair<Task::taskset, double>> solve();
};
