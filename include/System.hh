#pragma once

#include <vector>

#include "Task.hh"

class System
{
public:
	using taskset = Task::taskset;

	System();

	System(size_t n);

	virtual ~System();

	void init_total_work(std::function<double()> fn);

	void init_total_work(std::function<double(const Task &)> fn);

	void init_total_work(double p, double dp = 0);

	void init_total_work(size_t i, double p);

	void init_release_time(std::function<double()> fn);

	void init_release_time(std::function<double(const Task &)> fn);

	void init_release_time(double r, double dr = 0);

	const std::vector<Task> &tasks() const;

	size_t size() const;

	unsigned get_id() const;

	void set_id(unsigned id);

protected:
	unsigned m_id;

	std::vector<Task> m_tasks;
};

