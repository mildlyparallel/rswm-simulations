#pragma once

#include <vector>

#include "StrategyOffline.hh"
#include "SystemStationary.hh"

class StrategyFCS : public StrategyOffline
{
public:
	StrategyFCS();

	virtual ~StrategyFCS();

	virtual void start();

	virtual const char *name() const;

protected:

private:
};
